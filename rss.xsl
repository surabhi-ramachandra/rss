<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<html><body> 
			<xsl:for-each select="documentcollection/document/rss/channel">
			       <h2> <xsl:element name="a">
						  <xsl:attribute name="href"><xsl:value-of select="link" />
						  <xsl:text>.xml</xsl:text>       
						  </xsl:attribute>
						  <xsl:value-of select="title"/>
				    </xsl:element>     
				    <p>description - <xsl:value-of select="description"/></p></h2>
		       		 <xsl:for-each select="item">
		      		  <xsl:element name="a">
						  <xsl:attribute name="href"><xsl:value-of select="link" />
						  <xsl:text>.xml</xsl:text>       
						  </xsl:attribute>
						  <xsl:value-of select="title" />
				    </xsl:element>     
				    <p>description-<xsl:value-of select="description"/></p> 
				</xsl:for-each>
			</xsl:for-each>
			</body></html>
	</xsl:template>
</xsl:stylesheet>